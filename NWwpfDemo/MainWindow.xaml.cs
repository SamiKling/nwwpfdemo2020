﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NWwpfDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLinqDB_Click(object sender, RoutedEventArgs e)
        {
            NorthwindEntities db = new NorthwindEntities();
            var finnishCustomers = from cust in db.Customers
                                   where cust.Country == "Finland"
                                   select cust;

            foreach (var asiakas in finnishCustomers)
            {
                MessageBox.Show(asiakas.CompanyName);
            }

        }

        private void btnArray_Click(object sender, RoutedEventArgs e)
        {
            int[] numerot = { 5, 13, 8, 4, 10, 1, 9, 3, 5, 11, 6 };

            var suuretNumerot = from n in numerot
                                where n > 5
                                orderby n
                                select n;

            foreach (var numero in suuretNumerot)
            {
                MessageBox.Show(numero.ToString());
            }
        }

        private void btnLinqOliot_Click(object sender, RoutedEventArgs e)
        {
            List<Henkilo> henkilot = new List<Henkilo>()
            {
                new Henkilo()
                {
                    Nimi = "Paavo Pesusieni",
                    Osoite = "Kuja 11",
                    Ika = 34
                },
                new Henkilo()
                {
                    Nimi = "Liisa Pesusieni",
                    Osoite = "Kuja 11",
                    Ika = 33
                },
                new Henkilo()
                {
                    Nimi = "Pasi Pesusieni",
                    Osoite = "Kuja 11",
                    Ika = 11
                }
            };

            var henkiloLista = from hlo in henkilot
                               orderby hlo.Ika
                               select hlo;
            foreach (var henk in henkiloLista)
            {
                MessageBox.Show(henk.Nimi +"\n"+ henk.Osoite + "\n" + henk.Ika.ToString());
            }
        }
    }
}
